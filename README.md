Genshim Aranara music
==================================

Данный скрипт распознает одну из семи мелодий аранар и вопроизводит ее

Ниже представлен код скрипта

    // Genshim Aranara music

    // Процент точности распознования
    set #ac 70

    set SendExDelay 50  // задаржка между нажатиями нот
    set #out 0
    set $path "C:\Program Files\uopilot\img\"  // пусть к папке с изображениями песен
    while 0 = 0
        set $lpath "greate_dream_song.png"
        set $lpath $path$lpath
        set #n_target findimage (522, 65, 1359, 262 ($lpath) %target_crds 2 #ac 1 10 abs)
        if #n_target > 0
            sendex retrq
            set #out 1
        end_if

        set $lpath "song_of_the sprout.png"
        set $lpath $path$lpath
        set #n_target findimage (522, 65, 1359, 262 ($lpath) %target_crds 2 #ac 1 10 abs)
        if #n_target > 0
            sendex zbbnm
            set #out 1
        end_if

        set $lpath "dark_path.png"
        set $lpath $path$lpath
        set #n_target findimage (522, 65, 1359, 262 ($lpath) %target_crds 2 #ac 1 10 abs)
        if #n_target > 0
            sendex ewewy
            set #out 1
        end_if

        set $lpath "source_song.png"
        set $lpath $path$lpath
        set #n_target findimage (522, 65, 1359, 262 ($lpath) %target_crds 2 #ac 1 10 abs)
        if #n_target > 0
            sendex reurj
            set #out 1
        end_if

        set $lpath "animal_trail_song.png"
        set $lpath $path$lpath
        set #n_target findimage (522, 65, 1359, 262 ($lpath) %target_crds 2 #ac 1 10 abs)
        if #n_target > 0
            sendex zbncz
            set #out 1
        end_if

        set $lpath "vamadhi_song.png"
        set $lpath $path$lpath
        set #n_target findimage (522, 65, 1359, 262 ($lpath) %target_crds 2 #ac 1 10 abs)
        if #n_target > 0
            sendex reewq
            set #out 1
        end_if

        set $lpath "reborn_song.png"
        set $lpath $path$lpath
        set #n_target findimage (522, 65, 1359, 262 ($lpath) %target_crds 2 #ac 1 10 abs)
        if #n_target > 0
            sendex sambz
            set #out 1
        end_if

        if #out = 1
            end_script
        end_if
    end_while



